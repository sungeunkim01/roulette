// 2042714 Sungeun Kim
package Assignment01.roulette.code;
import java.util.Random;

public class RouletteWheel {

    // Create private field of type Random
    private Random random;
    // Creating a field type int to store number of the last spin.
    private int lastSpin;

    // Creating a constructor which initialize this to point at a new Random object
    // and set the num to store 0.
    public RouletteWheel() {
        this.random = new Random();
        this.lastSpin = 0;
    }

    // spin() method enable update the field number to store a new random int between 0 and 37,
    // and return void.
    public void spin() {
        lastSpin = random.nextInt(37);
    }

    // A getter method, getValue() returns and represents the value of the last spin.
    public int getValue() {
        return lastSpin;
    }
}


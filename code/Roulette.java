package Assignment01.roulette.code;
import java.util.Scanner;

// 2042714 Sungeun Kim

public class Roulette {
    public static void main (String[] args) {

        Scanner sc = new Scanner(System.in); 

        //Creating RouletteWheel object in the main method to use methods in RouletteWheel class
        RouletteWheel rouletteWheel = new RouletteWheel();
        
        // Initialize the starting bet for every user.
        int startingMoney = 1000;

        /* Starting a Roulette game while user bet more than the 0 and bet money is not bigger than 1000
         * If bet money is bigger than 1000, prints a message and end the game.
         * If user press Y to play a game, this asks which number he/she wants to bet on and amount he/she wants to bet.
         * Then, it calls spin() method from RouletteWheel to get a random number and also calls getValue() method to get 
         * the value of the number.
         * At the last, it finds winner/loser based on the picked random number.
        */
        while( startingMoney > 0 && startingMoney <= 1000) {
            System.out.println("Welcome to Roulette game! Do you want to play? (Y/N): ");
            String playGame = sc.next();

            if(playGame.equals("Y")) {
                System.out.println();
                System.out.println("Which number do you want to bet on? : ");
                int betNum = sc.nextInt();
                // Asking user how much he/she wants to bet.
                System.out.println("Enter amount you want to bet: ");
                int betAmount = sc.nextInt();
                System.out.println("-------------------------");

                if (betAmount > startingMoney) {
                    System.out.println("You can't bet that is more money than you have!");
                }
                System.err.println();
                // Calling spin() method from RouletteWheel class to spin the roulette, so a random number is plicked.
                rouletteWheel.spin();
                // Storing the number is picked from spin() method, and Print the number to the user
                int result = rouletteWheel.getValue();
                System.out.println("Spin Ressult: " + rouletteWheel.getValue());
                System.out.println("-------------------------");

                // Finding winner/loser with their money.
                if (result == betNum) {
                    int winMoney = betAmount * 35;
                    startingMoney += winMoney;
                    System.out.println();
                    System.out.println("Congrats, you won: $" + winMoney);
                } else {
                    startingMoney -= betAmount;
                    System.out.println();
                    System.out.println("You lost: $" + betAmount);
                }

            }

            // Quit the game if the user dose not want to play.
            else if (playGame.equals("N")){
                break;
            }
        }

        // Printing user's money after a game is played.
        System.out.println("****Game over****");
        System.out.println("You currently have: $" + startingMoney);
        
    }
}
